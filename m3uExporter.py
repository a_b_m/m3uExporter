import shutil
import os
import sys
# TODO: parallelism
# https://stackoverflow.com/questions/39623222/copying-a-file-to-multiple-paths-at-the-same-time
#



def copy_files(paths, sourcedir, outdir):
    filenames = []
    counter = 1
    for p in paths:
        path = os.path.join(sourcedir, p.strip())
        if os.path.isfile(path):
            filename = os.path.basename(path)
            filename = str(counter).zfill(3) + " " + filename
            if filename + "\n" in filenames:
                print(f"{filename} is already copied")
                continue
            f, ext = os.path.splitext(path)
            if ext == ".cue":
                shutil.copyfile(f+".flac", os.path.join(outdir, os.path.splitext(filename)[0]+".flac"))
            filenames.append(filename + "\n")
            print("Copying", filename)
            shutil.copyfile(path, os.path.join(outdir, filename))
            counter += 1
        else: 
            print(f"'{path}'' is not a valid filepath")
    return filenames


def create_playlist(filenames, dir, name):
    pl_file = open(os.path.join(dir, name), 'w')
    pl_file.writelines(filenames)
    pl_file.close()


def export_playlist(source_pl_path, output_directory):
    if not os.path.isfile(source_pl_path):
        raise Exception("There is no file under path {0}".format(source_pl_path))
    f, ext = os.path.splitext(source_pl_path)
    if ext != ".m3u" and ext != ".m3u8":
        raise Exception("Provided file is not m3u(8) playlist file")

    if not os.path.isdir(output_directory):
        os.makedirs(output_directory)
    pl_name = os.path.basename(source_pl_path)
    pl_dir = os.path.dirname(source_pl_path)
    source_pl = open(source_pl_path, 'r', encoding="utf-8")
    filepaths = source_pl.readlines()

    filenames = copy_files(filepaths, pl_dir, output_directory)
    create_playlist(filenames, output_directory, pl_name)
