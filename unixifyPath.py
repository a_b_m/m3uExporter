import sys
import os
import argparse
import shutil


parser = argparse.ArgumentParser(description=r"Convert Windows-style paths (music\genre\filename.mp3) to Unix-style paths (music/genre/filename.mp3)")
parser.add_argument('playlist', help="playlist file")


args = parser.parse_args()

shutil.copyfile(args.playlist, args.playlist + ".old")
pl_file = open(args.playlist, 'r+', encoding='ISO-8859-2')

source_paths = pl_file.readlines()
result_paths = []
for p in source_paths:
    result_paths.append(p.replace('\\', '/'))

pl_file.seek(0)
pl_file.writelines(result_paths)
pl_file.truncate()
pl_file.close()

