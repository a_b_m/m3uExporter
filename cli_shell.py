import argparse
import m3uExporter

parser = argparse.ArgumentParser(description="Export m3u(8) playlist")
parser.add_argument('playlist', help="source playlist file")
parser.add_argument('dir', help="output directory")

args = parser.parse_args()
# try:
print("Exporting...")
m3uExporter.export_playlist(args.playlist, args.dir)
print("Done")
# except Exception as ex:
    # print(ex)
