# m3uExporter - simple m3u and m3u8 playlist export tool

Requires python 3.x.

### Usage
`python cli_shell.py playlist_path output_directory`

### License 
This project is licensed under the [Do What the Fuck You Want to Public License](http://www.wtfpl.net/)